// ##Задание
// Нарисовать на странице круг используя параметры, которые введет пользователь.

let scriptTag = document.getElementsByTagName('script')[0];
const fragment = document.createDocumentFragment();

let drawCircle = () => {
    event.currentTarget.style.display = 'none';

    let circleDiaInputText = document.createElement('p');
    circleDiaInputText.className = "circleDataText";
    circleDiaInputText.textContent = "Enter circle diameter in millimeters. It should be a positive integer:";
    fragment.appendChild(circleDiaInputText);
    let circleDiaInput = document.createElement('input');
    circleDiaInput.className = "circleData";
    circleDiaInput.type = 'number';
    circleDiaInput.placeholder = "20 for example";
    fragment.appendChild(circleDiaInput);

    let circleColorInputText = document.createElement('p');
    circleColorInputText.className = "circleDataText";
    circleColorInputText.textContent = "Enter a color of a circle in 'color', RGB, HEX or HSL format:";
    fragment.appendChild(circleColorInputText);
    let circleColorInput = document.createElement('input');
    circleColorInput.className = "circleData";
    circleColorInput.placeholder = "hsl(132, 56%, 30%) for example";
    circleColorInput.type = 'text';
    fragment.appendChild(circleColorInput);

    let drawBtn = document.createElement('button');
    drawBtn.className = "drawCircleBtn";
    drawBtn.textContent = "TO DRAW";
    fragment.appendChild(drawBtn);

    document.body.insertBefore(fragment, scriptTag);

    let createCircle = () => {
        let circle = document.createElement('div');
        circle.className = "circle";
        circle.style.width = `${circleDiaInput.value}mm`;
        circle.style.height = `${circleDiaInput.value}mm`;
        circle.style.backgroundColor = `${circleColorInput.value}`;
        document.body.insertBefore(circle, scriptTag);
    };

    document.getElementsByClassName("drawCircleBtn")[0].addEventListener("click", createCircle);
};

document.getElementById("drawCircleBtn").addEventListener("click", drawCircle);